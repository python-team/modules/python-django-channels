Source: python-django-channels
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Michael Fladischer <fladi@debian.org>,
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
 pybuild-plugin-pyproject,
 python3-all,
 python3-asgiref (>= 3.2.10),
 python3-async-generator,
 python3-async-timeout,
 python3-daphne (>= 3),
 python3-django,
 python3-pytest,
 python3-pytest-asyncio,
 python3-pytest-django,
 python3-setuptools,
 python3-sphinx,
 python3-sphinx-rtd-theme,
Standards-Version: 4.7.0
Homepage: https://github.com/django/channels/
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-django-channels
Vcs-Git: https://salsa.debian.org/python-team/packages/python-django-channels.git
Testsuite: autopkgtest-pkg-python
Rules-Requires-Root: no

Package: python-django-channels-doc
Section: doc
Architecture: all
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Multi-Arch: foreign
Description: Developer-friendly asynchrony for Django (Documentation)
 Channels is a project to make Django able to handle more than just plain HTTP
 requests, including WebSockets and HTTP2, as well as the ability to run code
 after a response has been sent for things like thumbnailing or background
 calculation.
 .
 It’s an easy-to-understand extension of the Django view model, and easy to
 integrate and deploy.
 .
 This package contains the documentation.

Package: python3-django-channels
Architecture: all
Depends:
 ${misc:Depends},
 ${python3:Depends},
Suggests:
 daphne,
 python-django-channels-doc,
Description: Developer-friendly asynchrony for Django (Python3 version)
 Channels is a project to make Django able to handle more than just plain HTTP
 requests, including WebSockets and HTTP2, as well as the ability to run code
 after a response has been sent for things like thumbnailing or background
 calculation.
 .
 It’s an easy-to-understand extension of the Django view model, and easy to
 integrate and deploy.
 .
 This package contains the Python 3 version of the library.
